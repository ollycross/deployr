### 2.1.0 (2020-06-11)

##### Chores

* **vendors:**  NPM update ([c6f77cf3](https://gitlab.com/ollycross/deployr/commit/c6f77cf37fc3459bf4912d7ac7c0137ff669a4fc))
* **tidy:**  RM Debug ([5709cdcb](https://gitlab.com/ollycross/deployr/commit/5709cdcbd9c954879cd2558b28325e21ce774991))

##### New Features

* **deploy:**  CI deploy script ([b3f70a0e](https://gitlab.com/ollycross/deployr/commit/b3f70a0e3fe83db57c9e92c4beb5252f17f32a27))


#### 2.0.3 (2020-06-09)

##### Chores

* **refactoring:**  Add bin entries ([d293676e](https://gitlab.com/ollycross/deployr/commit/d293676e4e11bac2c94f38171fef1a90da9e0a28))


#### 2.0.2 (2020-06-09)

##### Chores

* **refactoring:**  Package name change ([54aa7667](https://gitlab.com/ollycross/deployr/commit/54aa76674dd09c3712e08bfd4bafc24dd4a49b66))


#### 2.0.1 (2020-06-09)

##### Chores

* **refactoring:**  Add .releasrrc ignore rules ([5ff0256a](https://gitlab.com/ollycross/deployr/commit/5ff0256ad4f7c3815c9983c74e767a251a14a794))


