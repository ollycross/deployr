# Deployr

In addition to the installed tools above, there is a suite of specific tools
available under the `deployr` command. These commands are available globally
and are accessible from the `gitlab-ci.yml` file in a project using this image.

```
# deployr help

Usage: deployr [options] [command]

Options:
  -V, --version                                    output the version number
  -d, --debug                                      Debug mode (default: false)
  -h, --help                                       display help for command

Commands:
  tarchive [options]                               Build a gzipped tar archive
  build-auth [options]                             Build auth.json file from environment
  wordpress [options] <user> <host> <releaseTag>   Deploy a Wordpress project
  release [options] <user> <host> <releaseTag>     Send the files to the desination server
  symlink [options] <user> <host> <link> <target>  Create a symlink on the destination server
  help [command]                                   display help for command

```

### `deployr tarchive`

```
# deployr help tarchive

Usage: deployr tarchive [options]

Build a gzipped tar archive

Options:
  -o, --output <output>            The output file (default: "build.tar.gz")
  -p, --path <path>                The path to archive (default: ".")
  -X, --ignore-file <ignore-file>  Distignore file.  Set to false to not use ignorefile` (default: ".distignore")
  -h, --help                       display help for command

This command will create a gzipped TAR archive.

If a `.distignore file` is created then these files (one per line) will be
excluded from the archive.  See https://www.gnu.org/software/tar/manual/html_node/exclude.html
```

### `deployr build-auth`

```
# deployr help build-auth

Usage: deployr build-auth [options]

Build auth.json file from environment

Options:
  -o, --output <output>  The output file (default: "./auth.json")
  -n, --do-not-write     If set, do not write to file, echo to console instead (default: false)
  -h, --help             display help for command

      This command will examine the environment for variables called 'COMPOSER_AUTH_example_HOST'.
      For each HOST it finds it will look for corresponding
      'COMPOSER_AUTH_example_USERNAME' and 'COMPOSER_AUTH_example_PASSWORD' entries.
      It will then build them into an object suitable for an auth.json file.
```

### `deployr release`

```
# deployr help release

Usage: deployr release [options] <user> <host> <releaseTag>

Send the files to the desination server

Options:
  -k, --key <key>             The private key to use (default: null)
  -l, --buildFile <path>      The built .TARed and Gzipped archive to send (default: "build.tar.gz")
  -r, --remotePath <path>     The remote path to deploy to (default: "/var/www")
  -h, --release-public <dir>  The release's public HTML folder, relative to root of --buildFile (default: "html")
  -H, --server-public <dir>   The server's public HTML folder, relative to --remotePath (default: "html")
  -h, --help                  display help for command

This command will send a release to the destination server.

The files are transferred via SSH, so the user running the command will need
to have access to the recieving server.  This can be done by passing the `-k`
options to specify a private key to provide.

The files will be transferred as a Gzipped, Tar archive and extracted on the
destination server.  This means that the destination server needs to be able to
run the `tar` command.

The release will be placed into the it's own folder
(under `[remotePath]/releases/[releaseTag]`) and the following symlinks will be created:
- `[remotePath]/releases/current` -> `[remotePath]/releases/[releaseTag]`
- `[remotePath]/releases/previous` -> `[remotePath]/releases/[prev releaseTag]`
- `[remotePath]/[serverPublic]` -> `[remotePath]/releases/current`

Everything but the current and previous releases will be removed from the
destination server.
```

### `deployr symlink`

```
# deployr help symlink

Usage: deployr symlink [options] <user> <host> <link> <target>

Create a symlink on the destination server

Options:
  -k, --key <key>  The private key to use (default: null)
  -h, --help       display help for command

This command will create a symlink on the server.

The files are transferred via SSH, so the user running the command will need
to have access to the recieving server.  This can be done by passing the `-k`
options to specify a private key to provide.

- If the link already exists, it will be removed and replaced.
- The directory structure to the link will be created, if necessary.
- The target directory will be created, if necessary.

```

### `deployr wordpress`

```
# deployr help wordpress

Usage: deployr wordpress [options] <user> <host> <releaseTag>

Deploy a Wordpress project

Options:
  -k, --key <key>             The private key to use (default: null)
  -l, --buildFile <path>      The built .TARed and Gzipped archive to send (default: "build.tar.gz")
  -r, --remotePath <path>     The remote path to deploy to (default: "/var/www")
  -h, --release-public <dir>  The release's public HTML folder, relative to root of --buildFile (default: "html")
  -H, --server-public <dir>   The server's public HTML folder, relative to --remotePath (default: "html")
  -h, --help                  display help for command

This command will send a release to the destination server, and do some
tasks specific to Wordpress projects.

See `deployr help release` for help pertaining to the `release` command.

In addition to releasing, this command will create a folder for files which
should be persisted between releases, which exists outside the release folder.
Symlinks will be created from the release's `assets` folder.  The following
folders will be persisted in this manner:
- `uploads`
- `languages`
- `shield`
- `cache`

The `persist` folder will be group writable.
```
