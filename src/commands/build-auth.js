const { writeFileSync } = require('fs');

const action = ({ output, doNotWrite }, logger) => {
  const auth = {
    'http-basic': {},
  };

  Object.entries(process.env)
    .filter(([key]) => key.match(/^COMPOSER_AUTH_[^_]+_HOST/))
    .forEach(([key, host]) => {
      auth['http-basic'][host] = {
        username: process.env[key.replace(/_HOST$/, '_USERNAME')] || '',
        password: process.env[key.replace(/_HOST$/, '_PASSWORD')] || '',
      };
    });

  logger.debug('Auth', auth);

  const authJson = JSON.stringify(auth);

  if (doNotWrite) {
    logger.info(authJson);
  } else {
    writeFileSync(output, authJson);
  }

  logger.debug(`Wrote auth to '${output}'`);
};

const init = (commander, initLogger) => {
  commander
    .command('build-auth')
    .description('Build auth.json file from environment')
    .on('--help', () => {
      /* eslint-disable-next-line no-console */
      console.log(`
This command will examine the environment for variables called 'COMPOSER_AUTH_example_HOST'.
For each HOST it finds it will look for corresponding
'COMPOSER_AUTH_example_USERNAME' and 'COMPOSER_AUTH_example_PASSWORD' entries.
It will then build them into an object suitable for an auth.json file.
    `);
    })
    .option('-o, --output <output>', 'The output file', './auth.json')
    .option(
      '-n, --do-not-write',
      'If set, do not write to file, echo to console instead',
      false,
    )
    .action(({ parent: { debug }, ...options }) =>
      action(options, initLogger(debug)),
    );
};

module.exports = { init, action };
