const { Client } = require('ssh2');
const getSSHConnectOptions = require('../utils/get-ssh-connect-options');

const transferBuild = require('../utils/transfer-build');
const manageReleases = require('../utils/manage-releases');
const remoteSymLink = require('../utils/remote-symlink');

const action = async (
  user,
  host,
  releaseTag,
  { buildFile, remotePath, serverPublic, releasePublic, key, password },
  logger,
) => {
  const conn = new Client();

  const connectOpts = getSSHConnectOptions(user, host, key, password);

  return new Promise((resolve, reject) => {
    conn
      .on('ready', async () => {
        try {
          const releasesDir = `${remotePath}/releases`;
          const destination = `${releasesDir}/${releaseTag}`;

          logger.info(`Uploading release to ${destination}`);
          // Transfer the new release to the server
          await transferBuild(conn, destination, buildFile, logger);

          // Sort out the current / previous symlinks in the releases folder,
          // and clean up any old releases
          await manageReleases(conn, releasesDir, logger);

          // Link the Public folder to the current release
          await remoteSymLink(
            conn,
            `${remotePath}/${serverPublic}`,
            `${releasesDir}/current/${releasePublic}`,
            false,
            logger,
          );

          resolve();
        } catch (e) {
          logger.error(e.message);
        } finally {
          conn.end();
        }
      })
      .on('error', (err) => {
        logger.error(err);
        reject(err);
      })
      .connect(connectOpts);
  });
};

const defineOptions = (command) => {
  return command
    .option('-p, --password <key>', 'The SSH user password', null)
    .option('-k, --key <key>', 'The SSH user private key', null)
    .option(
      '-l, --buildFile <path>',
      'The built .TARed and Gzipped archive to send',
      'build.tar.gz',
    )
    .option(
      '-r, --remotePath <path>',
      'The remote path to deploy to',
      '/var/www',
    )
    .option(
      '-h, --release-public <dir>',
      "The release's public HTML folder, relative to root of --buildFile",
      'html',
    )
    .option(
      '-H, --server-public <dir>',
      "The server's public HTML folder, relative to --remotePath",
      'html',
    );
};

const init = (commander, initLogger) => {
  const command = commander
    .command('release')
    .description('Send a release to the destination server')
    .on('--help', () => {
      /* eslint-disable-next-line no-console */
      console.log(`
This command will send a release to the destination server.

The files are transferred via SSH, so the user running the command will need
to have access to the recieving server.  This can be done by passing the \`-k\`
options to specify a private key to provide.

The files will be transferred as a Gzipped, Tar archive and extracted on the
destination server.  This means that the destination server needs to be able to
run the \`tar\` command.

The release will be placed into the it's own folder
(under \`[remotePath]/releases/[releaseTag]\`) and the following symlinks will be created:
- \`[remotePath]/releases/current\` -> \`[remotePath]/releases/[releaseTag]\`
- \`[remotePath]/releases/previous\` -> \`[remotePath]/releases/[prev releaseTag]\`
- \`[remotePath]/[serverPublic]\` -> \`[remotePath]/releases/current\`

Everything but the current and previous releases will be removed from the
destination server.

`);
    })
    .arguments('<user> <host> <releaseTag>')
    .action((user, host, releaseTag, { parent: { debug }, ...options }) =>
      action(user, host, releaseTag, options, initLogger(debug)),
    );
  defineOptions(command);
};

module.exports = { init, action, defineOptions };
