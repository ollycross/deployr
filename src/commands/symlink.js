const { Client } = require('ssh2');
const getSSHConnectOptions = require('../utils/get-ssh-connect-options');

const remoteSymLink = require('../utils/remote-symlink');

const action = async (
  user,
  host,
  link,
  target,
  { key, password, force },
  logger,
) => {
  const conn = new Client();

  const connectOpts = getSSHConnectOptions(user, host, key, password);

  conn
    .on('ready', async () => {
      try {
        await remoteSymLink(conn, link, target, force, logger);
      } catch (e) {
        logger.error(e.message);
      } finally {
        conn.end();
      }
    })
    .on('error', (err) => {
      logger.error(err);
    })
    .connect(connectOpts);
};

const init = (commander, initLogger) => {
  commander
    .command('symlink')
    .description('Create a symlink on the destination server')
    .on('--help', () => {
      /* eslint-disable-next-line no-console */
      console.log(`
This command will create a symlink on the server.

The files are transferred via SSH, so the user running the command will need
to have access to the recieving server.  This can be done by passing the \`-k\`
options to specify a private key to provide.

- If the link already exists, it will be removed and replaced.
- The directory structure to the link will be created, if necessary.
- The target directory will be created, if necessary.
`);
    })

    .arguments('<user> <host> <link> <target>')
    .option('-p, --password <key>', 'The SSH user password', null)
    .option('-k, --key <key>', 'The SSH user private key', null)
    .option('-F, --no-force', 'Do not force symlink creation', false)
    .action((user, host, link, target, { parent: { debug }, ...options }) =>
      action(user, host, link, target, options, initLogger(debug)),
    );
};

module.exports = { init, action };
