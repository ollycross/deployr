const tmp = require('tmp');
const { existsSync, copyFileSync } = require('fs');
const { spawn } = require('child_process');

const action = async ({ output, path, ignoreFile, debug }, logger) => {
  const createCommand = (tmpFile) => {
    const ignoreArgs = [];
    if (ignoreFile && existsSync(ignoreFile)) {
      ignoreArgs.push('-X');
      ignoreArgs.push(ignoreFile);
    }

    const args = [
      ...ignoreArgs,
      ...['--xform', 's:^./\\?::'],
      ...['-czv'],
      ...['-f', tmpFile, './'],
    ];

    return ['tar', args];
  };
  const { name: tmpFile } = tmp.fileSync();

  await new Promise((resolve, reject) => {
    const [tar, tarArgs] = createCommand(tmpFile);
    logger.info(`Creating archive \`${tmpFile}\` from \`${path}\`.`);
    logger.debug('Tar args:', tarArgs);
    const task = spawn(tar, tarArgs);

    task.stdout.on('data', (data) => {
      // if (debug) {
      //   /* eslint-disable no-console */
      //   console.log(data.toString());
      //   /* eslint-enable no-console */
      // }
    });

    task.stderr.on('data', (data) => {
      logger.error(data.toString());
    });

    task.on('close', (code) => {
      logger.debug(`Tar command result: ${code}`);
      resolve();
    });
    task.on('error', (err) => {
      logger.error(new Error(err.toString()));
      reject();
    });
  });

  logger.debug(`Moving tmp file \`${tmpFile}\` to \`${output}\`.`);
  copyFileSync(tmpFile, output);

  logger.info('Archiving complete');
};

const init = (commander, initLogger) => {
  commander
    .command('tarchive')
    .description('Build a gzipped tar archive')
    .on('--help', () => {
      /* eslint-disable-next-line no-console */
      console.log(`
This command will create a gzipped TAR archive.

If a \`.distignore file\` is created then these files (one per line) will be
excluded from the archive.  See https://www.gnu.org/software/tar/manual/html_node/exclude.html
An example can be found in the root of this project at \`.distignore.example\`.

`);
    })

    .option('-o, --output <output>', 'The output file', 'build.tar.gz')
    .option('-p, --path <path>', 'The path to archive', '.')
    .option(
      '-X, --ignore-file <ignore-file>',
      'Distignore file.  Set to false to not use ignorefile`',
      '.distignore',
    )
    .action(({ parent: { debug }, ...options }) =>
      action({ debug, ...options }, initLogger(debug)),
    );
};

module.exports = { init, action };
