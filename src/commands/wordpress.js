const { action: release, defineOptions } = require('./release');
const { action: symlink } = require('./symlink');
const asyncForEach = require('../utils/async-for-each');
const remoteChmod = require('../utils/remote-chmod');

const action = async (
  user,
  host,
  releaseTag,
  { buildFile, remotePath, serverPublic, releasePublic, key, password },
  logger,
) => {
  const publicPath = `${remotePath}/${serverPublic}`;
  const persistDir = `${remotePath}/persist`;

  const symlinkAssetsToPersist = async (folder) => {
    if (folder) {
      const assetsDir = `${remotePath}/releases/${releaseTag}/${releasePublic}/assets`;
      return symlink(
        user,
        host,
        `${assetsDir}/${folder}`,
        `${persistDir}/${folder}`,
        { key, password },
        logger,
      );
    }
    return false;
  };

  await release(
    user,
    host,
    releaseTag,
    { buildFile, remotePath, serverPublic, releasePublic, key, password },
    logger,
  );

  await asyncForEach(
    ['uploads', 'languages', 'shield', 'cache'],
    symlinkAssetsToPersist,
  );

  await remoteChmod(
    user,
    host,
    publicPath,
    '2755',
    '0644',
    logger,
    key,
    password,
  );
  await remoteChmod(
    user,
    host,
    persistDir,
    '2775',
    '0664',
    logger,
    key,
    password,
  );
};

const init = (commander, initLogger) => {
  const command = commander
    .command('wordpress')
    .description('Deploy a Wordpress project')
    .on('--help', () => {
      /* eslint-disable-next-line no-console */
      console.log(`
This command will send a release to the destination server, and do some
tasks specific to Wordpress projects.

See \`deployr help release\` for help pertaining to the \`release\` command.

In addition to releasing, this command will create a folder for files which
should be persisted between releases, which exists outside the release folder.
Symlinks will be created from the release's \`assets\` folder.  The following
folders will be persisted in this manner:
- \`uploads\`
- \`languages\`
- \`shield\`
- \`cache\`

The \`persist\` folder will be group writable.
`);
    })
    .arguments('<user> <host> <releaseTag>')
    .action((user, host, releaseTag, { parent: { debug }, ...options }) =>
      action(user, host, releaseTag, options, initLogger(debug)),
    );
  defineOptions(command);
};

module.exports = { init, action };
