const commander = require('commander');
const chillWinston = require('chill-winston');
const { exit, argv } = require('process');

const buildAuth = require('./commands/build-auth');
const release = require('./commands/release');
const symlink = require('./commands/symlink');
const wordpress = require('./commands/wordpress');
const tarchive = require('./commands/tarchive');
const { version } = require('../package.json');

commander.version(version).option('-d, --debug', 'Debug mode', false);

let logger;

const initLogger = (debug) => {
  if (!logger) {
    logger = chillWinston(debug ? 'debug' : 'info');
  }

  return logger;
};

tarchive.init(commander, initLogger);
buildAuth.init(commander, initLogger);
wordpress.init(commander, initLogger);
release.init(commander, initLogger);
symlink.init(commander, initLogger);

try {
  commander.parse(argv);
} catch (err) {
  logger.error(err.message, err);
  exit(err.code || 1);
}
