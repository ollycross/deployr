const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    /* eslint-disable no-await-in-loop */
    await callback(array[index], index, array);
    /* eslint-enable no-await-in-loop */
  }
};

module.exports = asyncForEach;
