const extractSSHConnectOptions = (conn) => {
  return {
    host: conn.config.host,
    username: conn.config.username,
    privateKey: conn.config.privateKey,
    password: conn.config.password,
  };
};

module.exports = extractSSHConnectOptions;
