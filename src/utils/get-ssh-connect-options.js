const getSSHConnectOptions = (username, host, privateKey, password) => {
  if (!privateKey && !password) {
    throw new Error('Must provide either key or password');
  }

  return {
    username,
    host,
    privateKey,
    password,
  };
};

module.exports = getSSHConnectOptions;
