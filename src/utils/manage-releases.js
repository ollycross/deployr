const SftpClient = require('ssh2-sftp-client');
const semverSort = require('semver-sort');
const asyncForEach = require('./async-for-each');
const remoteSymLink = require('./remote-symlink');
const extractSSHConnectOptions = require('../utils/extract-ssh-connect-options');

const manageReleases = async (conn, releasesDir, logger) => {
  const connConfig = extractSSHConnectOptions(conn);

  const sftp = new SftpClient();

  const getReleases = () =>
    new Promise((resolve, reject) => {
      logger.debug('Getting list of releases');
      sftp
        .connect(connConfig)
        .then(() => sftp.list(releasesDir))
        .then((dirs) => {
          const mapped = dirs
            .filter(({ type }) => type !== 'l')
            .map(({ name }) => name);

          let sorted = [...mapped];
          try {
            semverSort.desc(sorted);
          } catch (e) {
            sorted = [...mapped];
            sorted.sort();
            sorted.reverse();
          }

          resolve(sorted);
        })
        .then(() => sftp.end())
        .then(resolve)
        .catch((err) => {
          reject(err);
        });
    });

  const linkReleases = async (current, previous) =>
    asyncForEach(
      Object.entries({
        current,
        previous,
      }).filter(([, target]) => target),
      ([link, target]) => {
        logger.info(`Using ${target} for ${link}`);
        return remoteSymLink(
          conn,
          `${releasesDir}/${link}`,
          `${releasesDir}/${target}`,
          true,
          logger,
        );
      },
    );

  const removeReleases = async (releases = []) =>
    new Promise((resolve, reject) => {
      if (releases.length) {
        logger.info('Removing old releases');
        logger.debug('Removing: ', releases);
        const command = `rm -rf ${releases
          .map((release) => `'${releasesDir}/${release}'`)
          .join(' ')}`;

        conn.exec(command, (err) => {
          if (err) {
            reject(err);
          }

          resolve();
        });
      } else {
        resolve();
      }
    });

  const [current, previous, ...old] = await getReleases();
  await linkReleases(current, previous);
  await removeReleases(old);
};

module.exports = manageReleases;
