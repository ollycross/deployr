const { Client } = require('ssh2');
const getSSHConnectOptions = require('../utils/get-ssh-connect-options');

const remoteChmod = async (
  user,
  host,
  path,
  dirMask,
  fileMask,
  logger,
  key = null,
  password = null,
) => {
  const conn = new Client();

  const makeCommand = (type, mask) =>
    `find '${path}' -type '${type}' -exec chmod '${mask}' \\{\\} `;

  const doDirectories = () =>
    new Promise((resolve, reject) => {
      logger.debug(`Setting directories to \`${dirMask}\``);

      const command = makeCommand('d', dirMask);
      logger.debug(`Command:  \`${command}\``);

      conn.exec(command, (err) => {
        if (err) {
          reject(err);
        }

        resolve();
      });
    });

  const doFiles = () =>
    new Promise((resolve, reject) => {
      logger.debug(`Setting directories to \`${fileMask}\``);

      const command = makeCommand('f', fileMask);
      logger.debug(`Command:  \`${command}\``);

      conn.exec(command, (err) => {
        if (err) {
          reject(err);
        }

        resolve();
      });
    });

  const connectOpts = getSSHConnectOptions(user, host, key, password);

  conn
    .on('ready', async () => {
      logger.info(`Recursively setting permissions on ${path}`);
      await doDirectories();
      await doFiles();
      conn.end();
    })
    .connect(connectOpts);
};
module.exports = remoteChmod;
