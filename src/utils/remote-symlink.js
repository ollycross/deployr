const makeCommand = (link, target, force) => {
  const linkDir = link
    .split('/')
    .slice(0, -1)
    .join('/');

  return `rm -rf '${link}' && mkdir -p '${linkDir}' && ${
    force ? `mkdir -p '${target}' &&` : ''
  } ln -s${force ? 'nf' : ''} '${target}' '${link}'`;
};

const remoteSymlink = (conn, link, target, force, logger) =>
  new Promise((resolve, reject) => {
    const command = makeCommand(link, target, force);
    logger.info(`Linking ${target} to ${link}`);
    logger.debug(`Command: \`${command}\``);
    conn.exec(command, (err) => {
      if (err) {
        reject(err);
      }

      resolve();
    });
  });

module.exports = remoteSymlink;
