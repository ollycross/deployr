const SftpClient = require('ssh2-sftp-client');
const extractSSHConnectOptions = require('../utils/extract-ssh-connect-options');

const transferDir = async (conn, remotePath, buildFile, logger) => {
  const sftp = new SftpClient();

  const connConfig = extractSSHConnectOptions(conn);

  const putArchive = () =>
    new Promise((resolve, reject) => {
      let currentUploadPercentage = 0;
      sftp
        .connect(connConfig)
        .then(() => sftp.mkdir(remotePath, true))
        .then(() => logger.debug(`Made remote directory ${remotePath}`))
        .then(() => logger.info(`Uploading release...`))
        .then(() =>
          sftp.fastPut(buildFile, `${remotePath}/${buildFile}`, {
            step: (transferred, chunk, total) => {
              const percentage = Math.round((transferred / total) * 100);
              if (
                currentUploadPercentage !== 100 &&
                (percentage - currentUploadPercentage >= 5 || percentage >= 100)
              ) {
                /* eslint-disable no-console */
                console.log(
                  `Transferred ${transferred} of ${total} (${percentage}%)`,
                );
                /* eslint-enable no-console */
                currentUploadPercentage = percentage;
              }
            },
          }),
        )
        .then(() =>
          logger.info(`Transferred archive to '${remotePath}/${buildFile}'`),
        )
        .then(() => sftp.end())
        .then(resolve)
        .catch((err) => {
          reject(err);
        });
    });

  const extractArchive = () =>
    new Promise((resolve, reject) => {
      const command = `tar -C '${remotePath}' -xzf '${remotePath}/${buildFile}'`;
      logger.debug(`Extracting '${remotePath}/${buildFile}' to '${remotePath}`);
      logger.debug(`Command: \`${command}\`.`);
      conn.exec(command, (err) => {
        if (err) {
          logger.error(err);
          reject(err);
          return;
        }
        logger.info(`Extracted '${remotePath}/${buildFile}' to '${remotePath}`);
        resolve();
      });
    });

  const deleteArchive = () =>
    new Promise((resolve, reject) => {
      sftp
        .connect(connConfig)

        .then(() => sftp.delete(`${remotePath}/${buildFile}`))
        .then(() => logger.debug(`Deleted archive ${remotePath}/${buildFile}`))
        .then(() => sftp.end())
        .then(resolve)
        .catch((err) => {
          reject(err);
        });
    });

  await putArchive();
  await extractArchive();
  await deleteArchive();

  return true;
};

module.exports = transferDir;
